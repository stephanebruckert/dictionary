#ifndef _WORD_H_
#define _WORD_H_

class Word{
public:
	
	/*Constructor*/
	Word();
	/*Destructor*/
	~Word();
	/*add or replace the English word*/
	int InsertEnglish(char *aWord);
	/*add or replace the Finnish word*/
	int InsertFinnish(char *aWord);
	/*define the next word (the dictionnary will be a linked list of words)*/
	int SetNext(Word *aWord);
	int SetPrevious(Word *aWord);

	/*get the English word*/
	void GetEnglish(char *p);
	/*get the English word*/
	void GetFinnish(char *p);	

	/*get the next word in the dictionary*/
	Word * GetNext();
	/*get the previous word in the dictionary*/
	Word * GetPrevious();
	
	/*compare a word (used to search and sort in the dictionary)*/
	int Compare(const char *aWord);  
	
	/*Displays the object*/
	void Display();

	
private:
	char *pEnglish;
	char *pFinnish;
	Word *next;
	Word *previous;
};

#endif
