#include <cstring>
#include <ctype.h>
#include <string.h>
#include "word.h"
#include <iostream>

/* Implementation of the word class. */

#define Word_OK (0)
#define Word_too_big (-3)
#define Buffer (200)
#define Max_len (30)

Word::Word(){
    pEnglish = NULL;
    pFinnish = NULL;
    next = NULL;
	previous = NULL;
}

Word::~Word(){	
}

int Word::InsertFinnish(char *aWord)
{
	char* word;
	word = (char*) malloc(Buffer);
	if (word == NULL) exit(1);
	strcpy(word, aWord);
	
	pFinnish = word;
	
    return Word_OK;
}

int Word::InsertEnglish(char *aWord)
{
	char* word;
	word = (char*) malloc(Buffer);
	if (word == NULL) exit(1);
	strcpy(word, aWord);
	
	pEnglish = word;
	
    return Word_OK;
}

int Word::SetNext(Word *w) 
{
	next = w;
	return Word_OK;
}

int Word::SetPrevious(Word *w)
{
	
	previous = w;
	return Word_OK;
}

void Word::GetEnglish(char *p) {
	strcpy(p, pEnglish);
}

void Word::GetFinnish(char *p) {
	strcpy(p, pFinnish);
}

Word *Word::GetNext() 
{
	return next;
}

Word *Word::GetPrevious() 
{
	return previous;
}

void Word::Display() 
{
	std::cout << pEnglish << " - " << pFinnish << std::endl;
}


/*
 * Return 0 if the words are equal
 * Return -1 if this is smaller than aWord
 * Return 1 if this is higher than aWord
 */
int Word::Compare(const char *aWord)
{
	int size_this = sizeof(pEnglish);
	int size_param = sizeof(aWord);
	
	int min = (size_this < size_param) ? size_this : size_param;
	
	for (int i = 0; i < min; i++) 
	{
		if(pEnglish[i] < aWord[i])
			return -1;
		else if(pEnglish[i] > aWord[i])
			return 1;
	}
	
	if (size_this == size_param)
		return 0;
	else
		return (size_this = min) ? -1 : 1;
}

