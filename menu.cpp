#include <iostream>
#include "menu.h"

using namespace std;
/* Implementation of the menu class. */
void Menu::PrintHeader(){
	cout 
	<< "*************************************" << endl
	<< "****      Dictionary             ****" << endl
	<< "****      By Stéphane Bruckert   ****" << endl
	<< "****      ID: 1101644            ****" << endl;
}

void Menu::PrintMenu(){
	cout 
	<< "*************************************" << endl
	<< "***             MENU              ***" << endl
	<< "*************************************" << endl
	<< "S - Search"         << endl
	<< "I - Insert"         << endl
	<< "D - Delete"         << endl
	<< "A - Show all words" << endl
	<< "E - Exit and save"  << endl
	<< "  - Any other key to clear the screen" << endl
	<< "*************************************" << endl;
} 
