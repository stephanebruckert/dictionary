#ifndef _MENU_H_
#define _MENU_H_

//this class was created as an exercise and also to reduce the size of code of
//the main.
class Menu{
public:
	/*print the information about the program and the author.*/
	void PrintHeader();
	/*print the options for the user.*/
	void PrintMenu();
};

#endif
