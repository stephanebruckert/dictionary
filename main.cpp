#include <iostream>
#include <fstream>
#include "menu.h"
#include "word.h"

using namespace std;

#define Word_OK (0)
#define Word_Not_Found (-1)
#define Word_already_Exist (-2)
#define Word_too_big (-3)
#define Dictionary_empty (-4)
#define File_error (-5)
#define Memory_error (-6)
#define Word_Unknown_Error (-20)
#define Buffer (200)
#define Max_len (30)
#define FILE_NAME "dictionary.txt"

/* Search the word in the list and print it if found. 
 return an error code if not found or the dictionary is empty. */
int SearchWord();
/* Add a word in the list from user input.
 return an error code if the word already exist or if input is too long. */
int InsertWord();
/* Function used by InsertWord() and InitDictionary() to insert a new word in 
 the list at right position (order alphabetically by english word).
 return an error code if there is a memory allocation problem. */
int chainWord(char *en, char *fi);
/* Remove a word from the list.
 return an error code if word not found. */
int DeleteWord();
/* Show all the words in the dictionary. This was first a debug function; but
 since it works, I kept it. Considere that as an extra feature.
 return an error code if the dictionnary is empty. */
int ShowAllWord();
/* Print error if something goes wrong. */
void PrintError(int aErrorCode);
/* Read the saved words from the file and load it to the dictionary.
 return an error code if it don't manage to open the file. */
int InitDictionary();
/* Save the words in the file and clean the memory before leaving the program.
 return an error code if there is a problem with the file. */
int ExitDictionary();

//the first word of the dictionary that is shared with all functions in main
Word *w = NULL;
Word *tail = NULL;

int main()
{
    Menu m;
    m.PrintHeader();
    m.PrintMenu();
    PrintError(InitDictionary());
    int errorcode = 0;
    char c;
    cout << endl << "Choose option: ";
    cin >> c;
    fflush(stdin); //I want one option at the time, so flush the end of line
    while(c != 'e' && c != 'E'){
        switch(c){
            case 's':
            case 'S':
				errorcode = SearchWord();
				break;
            case 'i':
            case 'I':
				errorcode = InsertWord();
				break;
            case 'd':
            case 'D':
				errorcode = DeleteWord();
				break;
            case 'a':
            case 'A':
				errorcode = ShowAllWord();
				break;
            default:
				system("cls");
				m.PrintMenu();
        }
        PrintError(errorcode);
        cout << endl << "Choose option: ";
        cin >> c;
        fflush(stdin);
    }
    PrintError(ExitDictionary());
    //system("PAUSE"); sh: pause: command not found
    return 0;
}

int SearchWord(){
	if (w == NULL)
        return Dictionary_empty;

    Word *q;
    q = w;
    char in[Buffer] = {'a'};
	
    cout << "Search: " << endl;
	cin.ignore(numeric_limits<streamsize>::max(), '\n');
    cin.getline(in, Buffer);
	
    if (strlen(in) > Max_len)
        return Word_too_big;
	
    while (q != NULL)
    {
        if (q->Compare(in) == 0)
        {
            q->Display();
            return Word_OK;
        }
		
        q = q->GetNext();
    }
	
    return Word_Not_Found;
}

int InsertWord(){
	cin.ignore();
    cout << "Insertion" << endl << "Enter English Word: ";
    char in[Buffer] = {'a'};
    cin.getline(in, Buffer);
    if(strlen(in) > Max_len)
        return Word_too_big;
	
    cout << "Enter Finnish Word: ";
    char inF[Buffer] = {'a'};
    cin.getline(inF, Buffer);
    if(strlen(inF) > Max_len)
        return Word_too_big;
		
    int res = chainWord(in, inF);
    if(res == Word_OK)
        cout << "Word insert OK" << endl;
    return 0;
}


int chainWord(char *en, char *fi){
	//Temporary data
	Word *q;
	Word *tmp = NULL;
	tmp = new Word;
	
	//We create the object already
	tmp->InsertEnglish(en);
	tmp->InsertFinnish(fi);
	
    //If the dictionary is empty we just need to set the first word
    if(w == NULL)
	{
        tmp->SetPrevious(NULL);
		w = new Word;
        w->SetPrevious(tmp);
        w = tmp;
        return Word_OK;
    } 
	else 
	{ 
		if(tmp == NULL) //check memory
			return Memory_error;
		
		q = w;
		
		while (q != NULL)
		{
			int res = q->Compare(en);
		
			if (res == 1)
			{
				Word *prev;
				prev = q->GetPrevious();
				
				tmp->SetPrevious(prev);
				tmp->SetNext(q);
				
				if (prev != NULL)
					prev->SetNext(tmp);
				else
					w = tmp;
				q->SetPrevious(tmp);
			
				return Word_OK;
			} else if (res == 0)
				return Word_already_Exist;
		
			if (q->GetNext() == NULL)
                break;
            else
                q = q->GetNext();
		}
	
		// Add to the last
		q->SetNext(tmp);
		tmp->SetPrevious(q);
		
		return Word_OK;
	}
	
	return Word_OK;
}

int ShowAllWord() {
	Word *q = new Word;
	if (w == NULL)
	{
		cout << "The dictionary is empty" << endl;
		return 0;
	}
	q = w;
	cout << "All words:" << endl;
	int i = 1;
	while (q != NULL)
	{
		if (i % 10 == 0) 
			cout << "English - Finnish" << endl;
		
		cout << i << ") ";
		q->Display();
		q = q->GetNext();
		i++;
	}
	
	return 0;
}

int DeleteWord() {
	if (w == NULL)
        return Dictionary_empty;
	
    Word *q;
    q = w;
    char in[Buffer] = {'a'};
	
    cout << "Delete: " << endl;
	cin.ignore(numeric_limits<streamsize>::max(), '\n');
    cin.getline(in, Buffer);
	
    if (strlen(in) > Max_len)
        return Word_too_big;
	
    while (q != NULL)
    {
        if (q->Compare(in) == 0)
        {
            Word *prev;
			Word *next;

            prev = q->GetPrevious();
            next = q->GetNext();
			
			if (next != NULL) next->SetPrevious(prev);
            if (prev != NULL) prev->SetNext(next);
            else w = next;
			
            delete q;
			
            return Word_OK;
        }
        q = q->GetNext();
    }
	
    return Word_Not_Found;
	
}

void PrintError(int aErrorCode){
	switch(aErrorCode){
		case Word_OK:
            break;
		case Word_Not_Found:
            cout << "Word not found!" << endl;
            break;
		case Word_already_Exist:
            cout << "The word already exist!" << endl;
            break;
		case Word_too_big:
            cout << "The word is too big!" << endl;
            break;
		case Dictionary_empty:
            cout << "Dictionary is empty!" << endl;
            break;
		case File_error:
            cout << "Error with file!" << endl;
            break;
		case Memory_error:
            cout << "Memory allocation problem!" << endl;
            break;
		case Word_Unknown_Error:
		default:
            cout << "Oppps Unknown Error!" << endl;
            break;
	}                       
}     

int InitDictionary(){
    ifstream dicoFile (FILE_NAME);
    if(!dicoFile.is_open())
        return File_error;
    char in[Buffer] = {'a'};
    char inF[Buffer] = {'a'};
    int line = 0;
    while(!dicoFile.eof()){
        if(line == 0){
            //first line must be English word
            dicoFile.getline(in, Buffer);
            line++;
        }else if(line == 1){
            //second line must be Finnish word
            dicoFile.getline(inF, Buffer);
            line++;
        }else{
            //third line is empty line to separate words; so we should have Finnish and English words
            if(strlen(in) == 0 || strlen(in) > Max_len || strlen(inF) == 0 || strlen(inF) > Max_len){
                cout << "There is a word in file that is not valid";
            }else{
                PrintError(chainWord(in, inF));
            }
            dicoFile.getline(in, Buffer);//read the line to make the loop going forward
            line = 0;//reset the line counter (next line is an English word or the end of the file)
        }
    }
    cout << "Words from file added to dictionary successfuly!" << endl;
    return Word_OK;
}

int ExitDictionary(){
	ofstream dicoFile (FILE_NAME);
	if(!dicoFile.is_open())
        return File_error;
	
	int errorcode = 0;

	Word *q = new Word;
	if (w == NULL)
	{
		dicoFile << "";
		cout << "No words to save!";
	}
	else
	{
		q = w;
		while (q != NULL)
		{
			char in[Buffer] = {'a'};
			q->GetEnglish(in);
			char inF[Buffer] = {'a'};
			q->GetFinnish(inF);
			
			dicoFile << in << endl << inF << endl << endl;
			
			q = q->GetNext();
		}
		cout << "Successfully saved!";
	}
	cout << " Goodbye!" << endl << endl;
	
    //system("cls"); sh: cls: command not found
	
    return errorcode;
}
